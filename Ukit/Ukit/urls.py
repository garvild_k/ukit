from django.contrib import admin
from django.urls import path, include
from django.conf import urls
from pathlib import Path
from .api_exc_handler import api_exc_handler
from functools import partial

urlpatterns = [
    path('api/', include('api.urls')),
    path('admin/', admin.site.urls),
    path('', include('frontend.urls')),
]

# def is_subpath(subpath: Path, superpath: Path):
#     subparts = subpath.resolve().parts
#     superparts = superpath.resolve().parts
#     return superparts == subparts[:len(superparts)]
#
#
# def handlerxxx(status_code, request, *args, **kwargs):
#     if is_subpath(Path(request.path), Path(f"/{API_PATH}")):
#         return api_exc_handler(status_code, request, *args, **kwargs)
#     default_handler = getattr(urls, f"handler{status_code}")
#     return default_handler(request, *args, **kwargs)
#
#
# handler400 = partial(handlerxxx, 400)
# handler403 = partial(handlerxxx, 403)
# handler404 = partial(handlerxxx, 404)
# handler500 = partial(handlerxxx, 500)
