from rest_framework.exceptions import bad_request, server_error
from rest_framework.exceptions import status
from django.http import JsonResponse
# The quote function %-escapes all characters that are neither in the
# unreserved chars ("always safe") nor the additional chars set via the safe arg.
from urllib.parse import quote


def permission_denied(request, *args, **kwargs):
    return JsonResponse({'details': 'The requested resource was not found on this server.'},
                        status=status.HTTP_403_FORBIDDEN)


def page_not_found(request, *args, **kwargs):
    response_content = {
        'details': 'The requested resource was not found on this server.',
        'context': {
            'request_path': quote(request.path)
        }
    }
    try:
        exception = kwargs['exception']
    except KeyError:
        pass
    else:
        exc_info = repr(exception)
        response_content['context']['exception'] = exc_info
    return JsonResponse(response_content,
                        status=status.HTTP_404_NOT_FOUND)


def api_exc_handler(status_code, request, *args, **kwargs):
    handler = {
        400: bad_request,
        403: permission_denied,
        404: page_not_found,
        500: server_error
    }[status_code]
    return handler(request, *args, **kwargs)
